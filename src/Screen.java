import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Screen {

    public static void main(String[] args){
        new Window();
    }

    static class Window extends JFrame{
        static Container c;
        static JTextField field;
        static String text;
        static String DES_key;
        static byte[] DES_enc;
        static byte[] RSA_enc;
        static DSA dsa;
        static RSA rsa;

        Window(){
            super("SI");
            setSize(300,600);
            c = getContentPane();
            c.setLayout(new BorderLayout());
            setUI();
            setVisible(true);
        }

        private static void setUI(){

            JPanel panelForLabel = new JPanel();
            panelForLabel.setLayout(new BoxLayout(panelForLabel, BoxLayout.Y_AXIS));
            panelForLabel.setPreferredSize(new Dimension(300,500));

            JLabel label = new JLabel();
            label.setText("Enter String:");
            label.setSize(new Dimension(300,50));
            panelForLabel.add(label);


            field = new JTextField();
            field.setSize(new Dimension(300,50));
            field.setMaximumSize(new Dimension(300,50));
            field.setMinimumSize(new Dimension(300,50));
            field.setPreferredSize(new Dimension(300,50));
            panelForLabel.add(field);

            JButton button = new JButton();
            button.setText("Generate");
            button.setMaximumSize(new Dimension(300,50));
            button.setPreferredSize(new Dimension(300,50));
            button.setMinimumSize(new Dimension(300,50));
            panelForLabel.add(new JPanel());


            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    generate();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            panelForLabel.remove(panelForLabel.getComponentCount()-1);
                            panelForLabel.add( generateLayoutForKeys());
                            panelForLabel.invalidate();
                            panelForLabel.validate();
                        }
                    });
                }
            });

            c.add(panelForLabel,BorderLayout.NORTH);
            c.add(button,BorderLayout.SOUTH);
        }

        private static JPanel generateLayoutForKeys(){
            JPanel generatedKeys = new JPanel();
            generatedKeys.setLayout(new BoxLayout(generatedKeys, BoxLayout.Y_AXIS));
            generatedKeys.setPreferredSize(new Dimension(300,250));

            JLabel des_label = new JLabel();
            des_label.setPreferredSize(new Dimension(300,50));
            des_label.setText("DES key:");
            generatedKeys.add(des_label);

            JLabel des_key = new JLabel();
            des_key.setPreferredSize(new Dimension(300,50));
            des_key.setText(DES_key);
            generatedKeys.add(des_key);

            des_label = new JLabel();
            des_label.setPreferredSize(new Dimension(300,50));
            des_label.setText("DES encrypted:");
            generatedKeys.add(des_label);

            des_key = new JLabel();
            des_key.setPreferredSize(new Dimension(300,50));
            des_key.setText(DES_enc.toString());
            generatedKeys.add(des_key);

            des_label = new JLabel();
            des_label.setPreferredSize(new Dimension(300,50));
            des_label.setText("DES decrypted:");
            generatedKeys.add(des_label);

            des_key = new JLabel();
            des_key.setPreferredSize(new Dimension(300,50));
            des_key.setText(new String(DES.decrypt(DES_enc,DES_key.getBytes())));
            generatedKeys.add(des_key);



            JLabel rsa_label = new JLabel();
            rsa_label.setPreferredSize(new Dimension(300,50));
            rsa_label.setText("RSA encrypted:");
            generatedKeys.add(rsa_label);

            JLabel rsa_enc = new JLabel();
            rsa_enc.setPreferredSize(new Dimension(300,50));
            rsa_enc.setText(RSA_enc.toString());
            generatedKeys.add(rsa_enc);

            rsa_label = new JLabel();
            rsa_label.setPreferredSize(new Dimension(300,50));
            rsa_label.setText("RSA decrypted:");
            generatedKeys.add(rsa_label);

            rsa_enc = new JLabel();
            rsa_enc.setPreferredSize(new Dimension(300,50));
            rsa_enc.setText(new String(rsa.decrypt(RSA_enc)));
            generatedKeys.add(rsa_enc);

            JLabel dsa_label = new JLabel();
            dsa_label.setPreferredSize(new Dimension(300,50));
            dsa_label.setText("DSA verify:");
            generatedKeys.add(dsa_label);

            JLabel dsa_key = new JLabel();
            dsa_key.setPreferredSize(new Dimension(300,50));
            dsa_key.setText(String.valueOf(dsa.verify()));
            generatedKeys.add(dsa_key);

            return generatedKeys;
        }

        private static void generate(){
            Window.text = Window.field.getText();
            String desKey = "7qw8sd4h";
            DES_enc = DES.encrypt(Window.text.getBytes(),desKey.getBytes());
            DES_key = desKey;

            rsa = new RSA();
            RSA_enc = rsa.encrypt(Window.text.getBytes());

            dsa = new DSA(Window.text);
        }
    }
}
