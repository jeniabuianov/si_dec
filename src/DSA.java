import java.io.FileInputStream;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileNotFoundException;
import java.security.KeyPair;
import java.security.PublicKey;
import java.security.Signature;
import java.security.PrivateKey;
import java.security.SecureRandom;
import java.security.SignedObject;
import java.security.KeyPairGenerator;
import java.security.SignatureException;
import java.security.InvalidKeyException;
import java.security.NoSuchProviderException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class DSA
{

    private PrivateKey privateKey = null;//null
    private PublicKey  publicKey  = null;//null
    private final String ALGORITM_DSA = "DSA";
    private final String FILE_private = "private.key";
    private final String FILE_public  = "public.key" ;
    //KeyPairgenerator = 618;

    SignedObject signedObject;
    Signature signature;

    public SignedObject getSignedObject() {
        return signedObject;
    }

    public boolean verify(){
        try {
            return verifySignedObject(signedObject, publicKey);
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return  false;
    }

    public Signature getSignature() {
        return signature;
    }

    public String unsigned(){
        try {
            return (String) signedObject.getObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public DSA(String myText)
    {
        try {
            createKeys();
            saveKey(FILE_private, privateKey);
            saveKey(FILE_public , publicKey );

            privateKey = (PrivateKey) readKey(FILE_private);
            publicKey  = (PublicKey ) readKey(FILE_public );

            signedObject = createSignedObject(myText, privateKey);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    private SignedObject createSignedObject(final String message, PrivateKey key) throws InvalidKeyException,
            SignatureException, IOException, NoSuchAlgorithmException
    {
        signature = Signature.getInstance(key.getAlgorithm());
        return new SignedObject(message, key, signature);
    }
    private boolean verifySignedObject(final SignedObject signedObject, PublicKey key)
            throws InvalidKeyException, SignatureException, NoSuchAlgorithmException
    {
        // Verify the signed object
        Signature signature = Signature.getInstance(key.getAlgorithm());
        return signedObject.verify(key, signature);
    }

    //Generate private and public key
    private void createKeys() throws NoSuchAlgorithmException, NoSuchProviderException
    {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(ALGORITM_DSA);//SUN
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");//SUN
        keyPairGenerator.initialize(1024,random);//1024  //2048
        KeyPair keyPair = keyPairGenerator.generateKeyPair();
        privateKey = keyPair.getPrivate();
        publicKey  = keyPair.getPublic();
    }

    private void saveKey(final String filePath, final Object key)
            throws FileNotFoundException, IOException
    {
        if (key != null){
            FileOutputStream fos = new FileOutputStream(filePath);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(key);
            oos.close();
            fos.close();
        }
    }

    private Object readKey(final String filePath)
            throws FileNotFoundException, IOException, ClassNotFoundException
    {
        FileInputStream fis = new FileInputStream(filePath);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        return object;
    }
}